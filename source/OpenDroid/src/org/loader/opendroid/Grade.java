package org.loader.opendroid;

import org.loader.opendroid.db.OpenDroid;

public class Grade extends OpenDroid {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
